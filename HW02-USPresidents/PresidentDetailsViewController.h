//
//  PresidentDetailsViewController.h
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/18/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresidentDetailsViewController : UIViewController
@property (strong, nonatomic) NSString * myPresidentImage;
@property (strong, nonatomic) NSString * myPresidentName;
@property (strong, nonatomic) NSString * myPresidentDob;
@property (strong, nonatomic) NSString * myPresidentDod;
@property (strong, nonatomic) NSString * myPresidentPlaceOfDeath;
@property (strong, nonatomic) NSString * myPresidentServingDates;
@property (strong, nonatomic) NSString * myPresidentPartyAffiliation;
@property (strong, nonatomic) NSString * myPresidentPriorOccupations;
@end
