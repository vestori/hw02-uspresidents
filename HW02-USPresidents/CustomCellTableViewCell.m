//
//  CustomCellTableViewCell.m
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/17/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "CustomCellTableViewCell.h"

@implementation CustomCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
