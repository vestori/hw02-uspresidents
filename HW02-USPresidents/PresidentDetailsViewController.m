//
//  PresidentDetailsViewController.m
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/18/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "PresidentDetailsViewController.h"

@interface PresidentDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *lblPresidentImage;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentDob;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentDod;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentPlaceOfDeath;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentServeDates;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentParty;
@property (weak, nonatomic) IBOutlet UILabel *lblPresidentPriorOccupation;


@end

@implementation PresidentDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.myPresidentName;
    self.lblPresidentImage.image = [UIImage imageNamed:self.myPresidentImage];
    self.lblPresidentDob.text = self.myPresidentDob;
    self.lblPresidentDod.text = self.myPresidentDod;
    self.lblPresidentPlaceOfDeath.text = self.myPresidentPlaceOfDeath;
    self.lblPresidentParty.text = self.myPresidentPartyAffiliation;
    self.lblPresidentServeDates.text = self.myPresidentServingDates;
    self.lblPresidentPriorOccupation.text = self.myPresidentPriorOccupations;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
