//
//  CustomCellTableViewCell.h
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/17/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *presidentImage;
@property (weak, nonatomic) IBOutlet UILabel *presidentName;
@property (weak, nonatomic) IBOutlet UILabel *presidentParty;
@end
