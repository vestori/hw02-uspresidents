//
//  AppDelegate.h
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/14/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

