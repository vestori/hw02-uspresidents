//
//  MyPresidentsTableViewController.m
//  HW02-USPresidents
//
//  Created by Orlando Gotera on 11/17/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MyPresidentsTableViewController.h"
#import "CustomCellTableViewCell.h"
#import "PresidentDetailsViewController.h"

@interface MyPresidentsTableViewController () <UISearchBarDelegate>
@property(nonatomic, strong) NSDictionary * allPresidents;
@property(nonatomic, strong) NSMutableDictionary * filteredPresidents;
@property(nonatomic, strong) NSArray *keys;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic) BOOL isFiltered;
@end

@implementation MyPresidentsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //loading dictionary
    NSString *path = [[NSBundle mainBundle]pathForResource:@"USPresidents" ofType:@"plist"];
    self.allPresidents = [NSDictionary dictionaryWithContentsOfFile:path];
    self.keys = [self.allPresidents allKeys];
    self.isFiltered = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isFiltered) {
        self.keys = [self.filteredPresidents allKeys];
        return self.keys.count;
    } else {
        self.keys = [self.allPresidents allKeys];
        return self.keys.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 NSString *myCell = @"presidentCell";
    CustomCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCell forIndexPath:indexPath];
    if (self.isFiltered) {
        self.keys = [self.filteredPresidents allKeys];
        NSString *key = self.keys[indexPath.section];
        NSArray *keyValues = self.filteredPresidents[key];
        
        cell.presidentImage.image = [UIImage imageNamed:keyValues[7]];
        cell.presidentName.text = keyValues[0];
        cell.presidentParty.text = keyValues[5];
    
    } else {
        self.keys = [self.allPresidents allKeys];
        NSString *key = self.keys[indexPath.section];
        NSArray *keyValues = self.allPresidents[key];
        
        cell.presidentImage.image = [UIImage imageNamed:keyValues[7]];
        cell.presidentName.text = keyValues[0];
        cell.presidentParty.text = keyValues[5];
    }

    return cell;
    
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(searchText.length == 0) {
        self.isFiltered = false;
    } else {
        self.isFiltered = true;
        self.filteredPresidents = [[NSMutableDictionary alloc]init];
//
        for (NSString *president in self.keys) {
            NSRange nameRange = [president rangeOfString:searchText options:NSCaseInsensitiveSearch];
            //NSLog(@"President: %@", president);
            if (nameRange.location != NSNotFound){
                [self.filteredPresidents setObject: [NSArray arrayWithArray: self.allPresidents[president]] forKey:president] ;
                
            }
            
        }
//        NSLog(@"Filtered Presidents: %@", self.filteredPresidents);
    }
    
    [self.tableView reloadData];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"presidentDetails"]) {
        PresidentDetailsViewController *detailVC = [segue destinationViewController];
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        //        StateInfo *item = [self.usStates objectAtIndex:myIndexPath.row];
        NSString *key = self.keys[myIndexPath.section];
        NSArray *keyValues = self.allPresidents[key];
        detailVC.myPresidentImage = keyValues[7];
        detailVC.myPresidentName = keyValues[0];
        detailVC.myPresidentDob = keyValues[1];
        detailVC.myPresidentDod = keyValues[2];
        detailVC.myPresidentPlaceOfDeath = keyValues[3];
        detailVC.myPresidentServingDates = keyValues[4];
        detailVC.myPresidentPartyAffiliation = keyValues[5];
        detailVC.myPresidentPriorOccupations = keyValues[6];
    }
    
}


    
@end
    
